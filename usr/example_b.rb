require '../rb/active_ontology'

# primitive class
class Person < ActiveOntology::Base
  property :has_child => :person # object property
  property :gender => [:male, :female] # enumerated class
end

class Family < ActiveOntology::Base
  Parent.equivalent_to :person, :has_child => :person # defined class
  Woman.equivalent_to :person, Gender.female # defined class
  Mother.equivalent_to :parent, Gender.female # defined class

  def foo
    puts !:person.is_equivalent?(:mother, :woman.complement) # true
  end
end

Family.new.foo
