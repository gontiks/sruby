require '../rb/active_ontology'

# primitive class
class Person < ActiveOntology::Base
  property :has_child => :person # object property
  property :gender => [:male, :female] # enumerated class
end

# defined class
class Parent < ActiveOntology::Base
  equivalent_to :person, :has_child => :person
end

# defined class
class Woman < ActiveOntology::Base
  equivalent_to :person, Gender.female
end

# defined class
class Mother < ActiveOntology::Base
  equivalent_to :parent, Gender.female
end

class Family < ActiveOntology::Base
  def foo
    puts "\n"
    yield
    execute_query('Mother and not Woman')
    yield
    execute_query('Mother') # individuals: Jane_1
    yield
  end
end

Family.new.foo { puts "#{"="*60}#{"\n"*2}" }
