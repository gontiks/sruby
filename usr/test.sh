#!/bin/bash

for f1 in *.rb
do
    for f2 in ../config/*.yaml
    do
        jruby --ng $f1 $f2
    done
done
