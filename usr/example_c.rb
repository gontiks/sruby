require '../rb/active_ontology'

# primitive class
class Person < ActiveOntology::Base
  property :has_child => :person # object property
  property :gender => [:male, :female] # enumerated class
end

class Parent < ActiveOntology::Base; end # primitive class

class Family < ActiveOntology::Base
  Parent.equivalent_to :person, :has_child => :person # defined class
  Woman.equivalent_to :person, Gender.female # defined class
  Mother.equivalent_to :parent, Gender.female # defined class

  def foo
    puts "\n"
    yield
    :parent.is_subset?(:person) # true
    yield
    :person.is_subset?(:parent) # false
    :mother.is_subset?(:parent) # true
    yield
  end
end

Family.new.foo { puts "#{"="*60}#{"\n"*2}" }
