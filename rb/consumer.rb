require "#{File.dirname(__FILE__)}/filter"

class Consumer < Connection
  def start
    begin
      puts 'consumer started [press ctrl+c to exit]...'
      @queue.subscribe(:block => true) { |info, properties, message| puts message }
    rescue Interrupt => _
      @connection.close
      exit(0)
    end
  end
end

Consumer.new.start
