require 'rubygems'
require 'bunny'

module Filter
  def method_added(name)
    return if @filtering # do not add filters to original_ methods
    return if name.eql?(:initialize)
    @filtering = true
    alias_method("original_#{name}", name)

    define_method(name) do |*args|
      Filter.producer.send(caller[0])
      self.send("original_#{name}", *args)
    end

    @filtering = false
  end

  private
    def Filter.producer
      @producer ||= Producer.new
    end
end

class Connection
  NAME = :method_calls

  def initialize
    @connection = Bunny.new(:automatically_recover => false)
    @connection.start
    @channel = @connection.create_channel
    @queue = @channel.queue(NAME, :auto_delete => true)
  end
end

class Producer < Connection
  def initialize
    super()
    ObjectSpace.define_finalizer(self, proc { @connection.close })
  end

  def send(arg)
    @channel.default_exchange.publish(arg, :routing_key => @queue.name)
  end
end
