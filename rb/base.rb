module ActiveOntology
  class Base
    include Bridge

    CONFIG = '../config/sources.yaml'
    DATA = '../data/'

    def initialize
      populate(REXML::Document.new(File.new("#{DATA}#{Base.sources.fetch(:data)[:document]}"))) if Base.sources.include?(:data)
      Base.check_consistency

      ObjectSpace.define_finalizer(self, proc {
        puts "The ontology is consistent :)#{"\n"*2}"
        Base.bridge.explain_unsatisfiable_class
        Base.bridge.write_kb
      })
    end

    protected
      def execute_query(arg)
        Base.bridge.execute_query(arg)
      end

    private
      def concepts; @concepts ||= Base.bridge.get_concepts end
      def properties; @properties ||= Base.bridge.get_properties end

      def populate(element)
        element.elements.each do |child|
          concepts.keys.each do |concept|
            if concept.casecmp(child.name) == 0 && !child.attributes['id'].nil?
              Base.bridge.create_individual(concepts[concept], child.attributes['id'])
            end
          end

          properties.keys.each do |property|
            if property.casecmp(child.name) == 0 || property.casecmp("has#{child.name}") == 0
              Base.bridge.create_object_property_assertion(child.parent.attributes['id'], properties[property], child.text.capitalize)
            end
          end

          populate(child)
        end
      end

    class << Base
      attr_accessor :ontology_src, :sources
      attr_writer :loaded

      def bridge; @bridge ||= Bridge::Base.new end
      def loaded; @loaded ||= false end

      def const_missing(name)
        Object.const_set(name, Class.new {
          def self.equivalent_to(*args)
            Base.set_equivalent(Base.bridge.create_concept(self.name), *args)
          end
        })
      end

      def create_class(name, args)
        cls = Object.const_set(name.capitalize, Class.new)

        args.each do |arg|
          cls.class.send(:define_method, arg) { Object.const_get(arg.capitalize).new }
          Object.const_set(arg.capitalize, Class.new(cls)).send(:define_method, :parent) { self.class.ancestors[1].name }
        end
      end

      def property(arg)
        run {
          if arg.values[0].kind_of?(Array)
            property = "has#{arg.keys[0].capitalize}"
            bridge.create_enumerated_class(
                ancestors.first.name,
                property,
                arg.keys[0].capitalize,
                arg.values[0].map(&:capitalize))
            create_class arg.keys[0], arg.values[0]
            range = arg.keys[0]
          else
            property = arg.keys[0].propertize
            range = arg.values[0]
          end

          bridge.create_subclass(bridge.create_object_property(ancestors.first.name, property, range.capitalize))
        }
      end

      def equivalent_to(*args)
        set_equivalent(bridge.create_concept(ancestors.first.name), *args)
      end

      def set_equivalent(concept, *args)
        run {
          if args[1].kind_of?(Hash)
            bridge.create_defined_class(
                bridge.create_object_property(args[0].capitalize, args[1].keys[0].propertize, args[1].values[0].capitalize),
                concept)
          else
            bridge.create_defined_class_with_individual(
                concept,
                bridge.create_object_property(args[0].capitalize, "has#{args[1].parent}", args[1].parent),
                args[1].class.name)
          end
        }
      end

      def run
        load
        yield
        check_consistency
      end

      def load
        Base.loaded ? return : Base.loaded = true
        Base.sources = YAML.load_file(ARGV[0].nil? ? CONFIG : ARGV[0]).symbolize_keys!

        if Base.sources.include?(:ontology)
          Base.ontology_src = Ontology.new(Base.sources.fetch(:ontology))
          bridge.read_kb
          check_consistency
        else
          bridge.create_kb
        end
      end

      def check_consistency
        unless bridge.is_consistent?
          puts 'inconsistent ontology'
          abort
        end
      end
    end
  end
end
