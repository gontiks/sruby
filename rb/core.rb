class Array
  def hashize
    Hash[Array.new(self).map! { |item| item.toStringID.match(/(?<=#).*/).to_s }.zip(self)]
  end
end

class Hash
  def symbolize_keys!
    keys.each { |key| self[(key.to_sym rescue key)] = delete(key) }
    values.each { |value| value.symbolize_keys! if value.class == Hash }
    self
  end
end

class Symbol
  def complement
    ActiveOntology::Base.check_consistency
    Bridge::Base.owl_reasoner.getComplement(self.conceptize)
  end

  def is_equivalent?(*args)
    ActiveOntology::Base.check_consistency
    args.map! { |arg| arg.kind_of?(Symbol) ? arg.conceptize : arg }
    Bridge::Base.owl_reasoner.isEquivalent(Bridge::Base.owl_reasoner.getIntersection(args), self.conceptize)
  end

  def is_subset?(arg)
    ActiveOntology::Base.check_consistency
    temp1, temp2 = self.conceptize, arg.conceptize

    if Bridge::Base.owl_reasoner.isSubset(temp1, temp2)
      Bridge::Base.provider.explainSubclass(temp1, temp2)
      return true
    end

    false
  end

  def conceptize
    Bridge::Base.owl_reasoner.createConcept(self.to_s.camelize)
  end

  def propertize
    self.to_s.camelize(:lower)
  end
end
