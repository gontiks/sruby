gem 'activesupport', '=3.2.3'

require 'active_support/inflector'
require 'rexml/document'
require 'yaml'

module ActiveOntology
  PATH = File.dirname(__FILE__)
  require "#{PATH}/filter"
  require "#{PATH}/core"
  require "#{PATH}/bridge"
  require "#{PATH}/ontology"
  require "#{PATH}/base"
end
