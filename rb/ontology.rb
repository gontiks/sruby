module ActiveOntology
  class Ontology
    attr_reader :document, :save

    def initialize(arg)
      @document = arg.fetch(:document)
      @save = arg.fetch(:save)
    end
  end
end
