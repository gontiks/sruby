import java.io.PrintWriter

import com.clarkparsia.owlapi.explanation.PelletExplanation
import com.clarkparsia.owlapi.explanation.io.manchester.ManchesterSyntaxExplanationRenderer
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory

class ExplanationProvider
  def initialize(arg)
    PelletExplanation.setup
    @renderer = ManchesterSyntaxExplanationRenderer.new
    @renderer.startRendering(PrintWriter.new(java.lang.System.out))
    @explanation = PelletExplanation.new(PelletReasonerFactory.getInstance.createReasoner(arg))
  end

  def explain_subclass(*args)
    puts "Why is #{args[0].capitalize} a subset of #{args[1].capitalize}?#{"\n"*2}"
    @renderer.render(@explanation.getSubClassExplanations(args[0].conceptize, args[1].conceptize))
    @renderer.endRendering
  end

  def explain_unsatisfiable_class(arg)
    puts "Why is #{arg.to_s.camelize} an unsatisfiable concept?#{"\n"*2}"
    @renderer.render(@explanation.getUnsatisfiableExplanations(arg.conceptize))
    @renderer.endRendering
  end
end
