require 'java'

require '../lib/aterm-java-1.6.jar'
require '../lib/jgrapht-jdk1.5.jar'
require '../lib/owlapi-distribution-3.4.8.jar'
require '../lib/pellet-core.jar'
require '../lib/pellet-datatypes.jar'
require '../lib/pellet-el.jar'
require '../lib/pellet-explanation.jar'
require '../lib/pellet-owlapiv3.jar'
require '../lib/pellet-rules.jar'

$CLASSPATH << '../bin'
JavaUtilities.get_proxy_class('experimental.DLQueryReasoner')
JavaUtilities.get_proxy_class('experimental.ExplanationProvider')
JavaUtilities.get_proxy_class('experimental.OntologyManager')

# require "#{File.dirname(__FILE__)}/explanation_provider"

module Bridge
  KB = '../kb/'

  class Base
    extend Filter

    class << Base
      attr_accessor :manager

      def provider
        @provider ||= Java::Experimental::ExplanationProvider.new(Base.manager.getOntology)
      end

      def dl_reasoner
        @dl_reasoner ||= Java::Experimental::DLQueryReasoner.new(Base.manager.getOntology)
      end

      def owl_reasoner
        @owl_reasoner ||= Java::Experimental::OntologyReasoner.new(Base.manager.getOntology)
      end
    end

    def read_kb
      Base.manager = Java::Experimental::OntologyManager.new("#{Bridge::KB}#{ActiveOntology::Base.ontology_src.document}")
    end

    def create_kb
      Base.manager = Java::Experimental::OntologyManager.new
    end

    def create_concept(name)
      Base.manager.createConcept(name)
    end

    def create_object_property(domain, property, range)
      Base.manager.createObjectProperty(domain, property, range)
    end

    def create_subclass(property)
      Base.manager.createSubclass(property)
    end

    def create_defined_class(property, concept)
      Base.manager.createDefinedClass(property, concept)
    end

    def create_individual(type, name)
      Base.manager.createIndividual(type, name)
    end

    def create_object_property_assertion(individual, property, obj)
      Base.manager.createObjectPropertyAssertion(individual, property, obj)
    end

    def create_enumerated_class(enumerated, property, domain, individuals)
      Base.manager.createEnumeratedClass(enumerated, property, domain, individuals)
    end

    def create_defined_class_with_individual(defined, property, individual)
      Base.manager.createDefinedClassWithIndividual(defined, property, individual)
    end

    def is_consistent?
      Base.manager.checkConsistency
    end

    def write_kb
      Base.manager.saveOntology(Bridge::KB)
    end

    def execute_query(arg)
      Base.dl_reasoner.execute_query(arg)
    end

    def explain_unsatisfiable_class
      Base.owl_reasoner.getUnsatisfiableConcepts.each { |concept| Base.provider.explainUnsatisfiableClass(concept) }
    end

    def get_concepts
      Array.new(Base.manager.getOntology.getClassesInSignature).hashize
    end

    def get_properties
      Array.new(Base.manager.getOntology.getObjectPropertiesInSignature).hashize
    end
  end
end
