package experimental;

import java.io.File;
import java.io.IOException;
// import java.util.logging.Logger;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.SetOntologyID;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class OntologyManager {
    // private static final Logger LOGGER = Logger.getLogger(OntologyReader.class.getName());
    private static final String DEFAULT_IRI = "http://www.semanticweb.org/ontologies/new",
            DIR = "kb/";

    private OWLOntologyManager manager;
    private OWLDataFactory factory;
    private IRI iri;
    private OWLOntology ontology;

    public OntologyManager() throws OWLOntologyCreationException {
        load();
        iri = IRI.create(DEFAULT_IRI);
        ontology = manager.createOntology(iri);
    }

    public OntologyManager(String file) throws OWLOntologyCreationException {
        load();
        ontology = manager.loadOntologyFromOntologyDocument(new File(file));
        iri = ontology.getOntologyID().getOntologyIRI();
    }

    public OWLOntology getOntology() { return ontology; }

    private void load() {
        manager = OWLManager.createOWLOntologyManager();
        factory = manager.getOWLDataFactory();
    }

    public void createOntology() {
        manager.applyChange(new SetOntologyID(ontology, new OWLOntologyID(iri, IRI.create(iri + "/version1"))));
    }

    public OWLClass createConcept(String name) {
        OWLClass cls = factory.getOWLClass(IRI.create(iri + "#" + name));
        manager.addAxiom(ontology, factory.getOWLDeclarationAxiom(cls));
        return cls;
    }

    public ObjectProperty createObjectProperty(String domainName, String propertyName, String rangeName) {
        return new ObjectProperty(domainName, propertyName, rangeName);
    }

    public void createSubclass(ObjectProperty property) {
        OWLObjectAllValuesFrom superClass = factory.getOWLObjectAllValuesFrom(property.getProperty(), property.getRange());
        manager.addAxiom(ontology, factory.getOWLSubClassOfAxiom(property.getDomain(), superClass));
    }

    public void createDefinedClass(ObjectProperty property, OWLClass defined) {
        OWLObjectSomeValuesFrom anonymous = factory.getOWLObjectSomeValuesFrom(property.getProperty(), property.getDomain());
        OWLObjectIntersectionOf intersection = factory.getOWLObjectIntersectionOf(property.getDomain(), anonymous);
        manager.addAxiom(ontology, factory.getOWLEquivalentClassesAxiom(defined, intersection));
    }

    public OWLNamedIndividual createIndividual(OWLClass type, String name) {
        OWLNamedIndividual individual = factory.getOWLNamedIndividual(IRI.create(iri + "#" + name));
        manager.addAxiom(ontology, factory.getOWLDeclarationAxiom(individual));
        manager.addAxiom(ontology, factory.getOWLClassAssertionAxiom(type, individual));
        return individual;
    }

    public void createObjectPropertyAssertion(OWLIndividual individual, ObjectProperty property, OWLIndividual obj) {
        manager.addAxiom(ontology, factory.getOWLObjectPropertyAssertionAxiom(property.getProperty(), individual, obj));
    }

    public void createObjectPropertyAssertion(String individual, OWLObjectProperty property, String obj) {
        manager.addAxiom(ontology, factory.getOWLObjectPropertyAssertionAxiom(property,
                factory.getOWLNamedIndividual(IRI.create(iri + "#" + individual)),
                factory.getOWLNamedIndividual(IRI.create(iri + "#" + obj))));
    }

    public void createEnumeratedClass(String domainName, String propertyName, String enumeratedName, String[] individualNames) {
        OWLClass enumerated = createConcept(enumeratedName);
        createObjectProperty(domainName, propertyName, enumeratedName);
        OWLNamedIndividual[] individuals = new OWLNamedIndividual[individualNames.length]; 
        for (int i = 0; i < individualNames.length; i++) individuals[i] = createIndividual(enumerated, individualNames[i]);
        manager.addAxiom(ontology, factory.getOWLEquivalentClassesAxiom(enumerated, factory.getOWLObjectOneOf(individuals)));
    }

    public void createDefinedClassWithIndividual(OWLClass defined, ObjectProperty property, String individualName) {
        OWLNamedIndividual individual = createIndividual(property.getRange(), individualName);
        OWLObjectHasValue anonymous = factory.getOWLObjectHasValue(property.getProperty(), individual);
        OWLObjectIntersectionOf intersection = factory.getOWLObjectIntersectionOf(property.getDomain(), anonymous);
        manager.addAxiom(ontology, factory.getOWLEquivalentClassesAxiom(defined, intersection));
    }

    public boolean checkConsistency() {
        OWLReasoner reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
        reasoner.precomputeInferences();
        // LOGGER.info("consistent ontology: " + reasoner.isConsistent());
        return reasoner.isConsistent();
    }

    public void saveOntology(String dir) throws OWLOntologyStorageException, IOException {
        OWLOntologyFormat format = manager.getOntologyFormat(ontology);
        OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();
        if (format.isPrefixOWLOntologyFormat()) owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
        manager.saveOntology(ontology, owlxmlFormat, IRI.create(File.createTempFile(System.currentTimeMillis() + "_", ".owl", new File(dir))));
    }

    public static void main(String[] args) throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
        OntologyManager creator = new OntologyManager();
        // create an ontology
        creator.createOntology();
        // create a class
        OWLClass person = creator.createConcept("Person");
        // create an object property
        ObjectProperty hasChild = creator.createObjectProperty("Person", "hasChild", "Person");
        // create a subclass
        creator.createSubclass(hasChild);
        // create a defined class
        OWLClass parent = creator.createConcept("Parent");
        creator.createDefinedClass(hasChild, parent);
        // create an individual
        OWLIndividual john = creator.createIndividual(person, "John_1");
        // create an enumerated class
        creator.createEnumeratedClass("Person", "hasGender", "Gender", new String[] {"Male", "Female"});
        ObjectProperty hasGender = creator.createObjectProperty("Person", "hasGender", "Gender");
        creator.createSubclass(hasGender);
        // create an object property assertion
        OWLIndividual male = creator.createIndividual(creator.createConcept("Gender"), "Male");
        creator.createObjectPropertyAssertion(john, hasGender, male);
        creator.createObjectPropertyAssertion("john", hasGender.getProperty(), "male");
        // create a defined class
        OWLClass woman = creator.createConcept("Woman");
        creator.createDefinedClassWithIndividual(woman, hasGender, "Female");
        // check the consistency of, and save, the ontology
        creator.checkConsistency();
        creator.saveOntology(DIR);
    }

    public class ObjectProperty {
        private OWLObjectProperty property;
        private OWLClass domain, range;

        public ObjectProperty(String domainName, String propertyName, String rangeName) {
            property = factory.getOWLObjectProperty(IRI.create(iri + "#" + propertyName));
            domain = factory.getOWLClass(IRI.create(iri + "#" + domainName));
            range = factory.getOWLClass(IRI.create(iri + "#" + rangeName));
            manager.addAxiom(ontology, factory.getOWLDeclarationAxiom(property));
            manager.addAxiom(ontology, factory.getOWLObjectPropertyDomainAxiom(property, domain));
            manager.addAxiom(ontology, factory.getOWLObjectPropertyRangeAxiom(property, range));
        }

        public OWLObjectProperty getProperty() { return property; }
        public OWLClass getDomain() { return domain; }
        public OWLClass getRange() { return range; }
    }
}
