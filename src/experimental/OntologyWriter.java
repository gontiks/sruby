package experimental;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class OntologyWriter {
    private static final Logger LOGGER = Logger.getLogger(OntologyReader.class.getName()); 
    private static final String DIR = "kb/",
            REMOTE_IRI = "http://www0.cs.ucl.ac.uk/staff/f.zervoudakis/documents/complex_missions.owl";

    public static void main(String[] args) throws OWLOntologyCreationException, IOException, OWLOntologyStorageException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(IRI.create(REMOTE_IRI));
        LOGGER.info("remote ontology: " + ontology);
        OWLOntologyFormat format = manager.getOntologyFormat(ontology);
        OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();
        if (format.isPrefixOWLOntologyFormat()) owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
        manager.saveOntology(ontology, owlxmlFormat, IRI.create(File.createTempFile("tmp_", ".owl", new File(DIR)).toURI()));
    }
}
