package experimental;

import java.io.File;
import java.util.logging.Logger;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;

public class ConsistencyChecker {
    private static final Logger LOGGER = Logger.getLogger(OntologyReader.class.getName());
    private static final String LOCAL_IRI = "kb/family.owl";

    public static void main(String[] args) throws OWLOntologyCreationException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(LOCAL_IRI));
        LOGGER.info("local ontology: " + ontology);
        OWLReasoner reasoner = new StructuralReasonerFactory().createReasoner(
                ontology, new SimpleConfiguration(new ConsoleProgressMonitor()));
        reasoner.precomputeInferences();
        LOGGER.info("consistent ontology: " + reasoner.isConsistent());
    }
}
