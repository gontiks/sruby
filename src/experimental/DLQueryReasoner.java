package experimental;

import java.util.Set;

import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.BidirectionalShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import com.clarkparsia.owlapiv3.OWL;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class DLQueryReasoner {
    private static final String LOCAL_IRI = "file:kb/family.owl";

    private OWLOntology ontology;
    private OWLReasoner reasoner;
    private BidirectionalShortFormProvider provider;

    public DLQueryReasoner(OWLOntology ontology) throws OWLOntologyCreationException {
        this.ontology = ontology; 
        reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
        provider = new BidirectionalShortFormProviderAdapter(
                OWL.manager, ontology.getImportsClosure(), new SimpleShortFormProvider());
    }

    public void executeQuery(String query) {
        System.out.println("Query: " + query + "\n");
        printEntities("ancestor classes", reasoner.getSuperClasses(parse(query), false).getFlattened());
        OWLClassExpression expression = parse(query);
        Node<OWLClass> classes = reasoner.getEquivalentClasses(expression);
        Set<OWLClass> entities = expression.isAnonymous()
                ? classes.getEntities() : classes.getEntitiesMinus(expression.asOWLClass());
        printEntities("equivalent classes", entities);
        printEntities("subclasses", reasoner.getSubClasses(parse(query), false).getFlattened());
        printEntities("individuals", reasoner.getInstances(parse(query), false).getFlattened());
    }

    private OWLClassExpression parse(String query) {
        ManchesterOWLSyntaxEditorParser parser = new ManchesterOWLSyntaxEditorParser(
                ontology.getOWLOntologyManager().getOWLDataFactory(), query);
        parser.setDefaultOntology(ontology);
        parser.setOWLEntityChecker(new ShortFormEntityChecker(provider));
        return parser.parseClassExpression();
    }

    private void printEntities(String name, Set<? extends OWLEntity> entities) {
        System.out.println(name + ": ");
        for (OWLEntity entity : entities) System.out.println(" - " + provider.getShortForm(entity));
        System.out.print("\n");
    }

    public static void main(String[] args) throws OWLOntologyCreationException {
        new DLQueryReasoner(OWL.manager.loadOntology(IRI.create(LOCAL_IRI))).executeQuery("Woman and not Mother");
    }
}
