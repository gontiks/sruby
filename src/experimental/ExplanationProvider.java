package experimental;

import java.io.IOException;
import java.io.PrintWriter;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import com.clarkparsia.owlapi.explanation.PelletExplanation;
import com.clarkparsia.owlapi.explanation.io.manchester.ManchesterSyntaxExplanationRenderer;
import com.clarkparsia.owlapiv3.OWL;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class ExplanationProvider {
    private static final String LOCAL_IRI = "file:kb/family.owl",
            NAMESPACE = "http://www.semanticweb.org/ontologies/family.owl";

    private ManchesterSyntaxExplanationRenderer renderer;
    private PelletExplanation explanation;
    private SimpleShortFormProvider provider;

    public ExplanationProvider(OWLOntology ontology) {
        PelletExplanation.setup();
        renderer = new ManchesterSyntaxExplanationRenderer();
        renderer.startRendering(new PrintWriter(System.out));
        explanation = new PelletExplanation(PelletReasonerFactory.getInstance().createReasoner(ontology));
        provider = new SimpleShortFormProvider();
    }

    public void explainSubclass(OWLClass subclass, OWLClass superclass) throws OWLException, IOException {
        System.out.println("Why is " + provider.getShortForm(subclass) + " a subset of " + provider.getShortForm(superclass) + "?");
        renderer.render(explanation.getSubClassExplanations(subclass, superclass));
        renderer.endRendering();
    }

    public void explainUnsatisfiableClass(OWLClass cls) throws OWLException, IOException {
        System.out.println("Why is " + provider.getShortForm(cls) + " an unsatisfiable concept?");
        renderer.render(explanation.getUnsatisfiableExplanations(cls));
        renderer.endRendering();
    }

    public static void main(String[] args) throws OWLException, IOException {
        OWLClass mother = OWL.Class(NAMESPACE + "#Mother");
        OWLClass person = OWL.Class(NAMESPACE + "#Person");
        ExplanationProvider provider = new ExplanationProvider(OWL.manager.loadOntology(IRI.create(LOCAL_IRI)));
        provider.explainSubclass(mother, person);
        mother = OWL.Class(NAMESPACE + "#UnsatisfiableMother");
        provider.explainUnsatisfiableClass(mother);
    }
}
