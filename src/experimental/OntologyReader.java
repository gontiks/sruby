package experimental;

import java.io.File;
import java.util.logging.Logger;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class OntologyReader {
    private static final Logger LOGGER = Logger.getLogger(OntologyReader.class.getName());
    private static final String LOCAL_IRI = "kb/family.owl",
            REMOTE_IRI = "http://www0.cs.ucl.ac.uk/staff/f.zervoudakis/documents/complex_missions.owl";

    public static void main(String[] args) throws OWLOntologyCreationException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(LOCAL_IRI));
        LOGGER.info("local ontology: " + ontology);
        manager.removeOntology(ontology);
        ontology = manager.loadOntologyFromOntologyDocument(IRI.create(REMOTE_IRI));
        LOGGER.info("remote ontology: " + ontology);
    }
}
