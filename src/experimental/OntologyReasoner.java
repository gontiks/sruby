package experimental;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Logger;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import com.clarkparsia.owlapiv3.OWL;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class OntologyReasoner {
    private static final Logger LOGGER = Logger.getLogger(OntologyReader.class.getName());
    private static final String FILE = "family.owl",
            LOCAL_IRI = "file:kb/" + FILE;

    private OWLOntology ontology;
    private OWLReasoner reasoner;
    private OWLDataFactory factory;
    private IRI iri;

    public OntologyReasoner() throws OWLOntologyCreationException {
        ontology = OWL.manager.loadOntology(IRI.create(LOCAL_IRI));
        load();
    }

    public OntologyReasoner(OWLOntology ontology) {
        this.ontology = ontology;
        load();
    }

    private void load() {
        reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
        factory = OWL.manager.getOWLDataFactory();
        iri = ontology.getOntologyID().getOntologyIRI();
    }

    public OWLClass createConcept(String name) {
        OWLClass cls = factory.getOWLClass(IRI.create(iri + "#" + name));
        OWL.manager.addAxiom(ontology, factory.getOWLDeclarationAxiom(cls));
        return cls;
    }

    public boolean isEquivalent(OWLClassExpression cls1, OWLClass cls2) {
        return isNode(reasoner.getEquivalentClasses(cls1).getEntities().toArray(), cls2);
    }

    public boolean isSubset(OWLClass subClass, OWLClass superClass) {
        Set<Node<OWLClass>> nodes = reasoner.getSuperClasses(subClass, false).getNodes();
        ArrayList<Object> objects = new ArrayList<Object>();
        for (Node<OWLClass> node : nodes) objects.addAll(node.getEntities());
        return isNode(objects.toArray(), superClass);
    }

    private boolean isNode(Object[] classes, OWLClass cls) {
        for (int i = 0; i < classes.length; i++) {
            if (cls.getIRI().toString().equals(((OWLClass) classes[i]).getIRI().toString())) return true;
        }
        return false;
    }

    public OWLClass getComplement(OWLClass cls) {
        return factory.getOWLObjectComplementOf(cls).getOperand().asOWLClass();
    }

    public OWLObjectIntersectionOf getIntersection(OWLClassExpression[] expressions) {
        return factory.getOWLObjectIntersectionOf(expressions);
    }

    public Object[] getUnsatisfiableConcepts() {
        return reasoner.getUnsatisfiableClasses().getEntitiesMinusBottom().toArray();
    }

    public static void main(String[] args) throws OWLOntologyCreationException {
        OntologyReasoner reasoner  = new OntologyReasoner();
        // reason about equivalence
        OWLClass parent = reasoner.createConcept("Parent"), person = reasoner.createConcept("Person");
        LOGGER.info("Parent is equivalent to Person: " + reasoner.isEquivalent(parent, person)); // false
        OWLClass familyMember = reasoner.createConcept("FamilyMember"), pet = reasoner.createConcept("Pet");
        LOGGER.info("FamilyMember is equivalent to Pet: " + reasoner.isEquivalent(familyMember, pet)); // true
        // reason about subsumption
        LOGGER.info("Parent is a subset of Person: " + reasoner.isSubset(parent, person)); // true
        // reason about logical negation
        OWLClass mother = reasoner.createConcept("Mother");
        LOGGER.info("Person is not equivalent to complement of Mother: " +
                reasoner.isEquivalent(person, reasoner.getComplement(mother))); // false
        // reason about equivalence against an intersection
        OWLClass woman = reasoner.createConcept("Woman");
        OWLClassExpression[] expressions = new OWLClassExpression[] {woman, reasoner.getComplement(mother)};
        LOGGER.info("Person is equivalent to the intersection of Woman and the complement of Mother: " +
                reasoner.isEquivalent(reasoner.getIntersection(expressions), person)); // false
    }
}
