package experimental;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import experimental.OntologyManager.ObjectProperty;

public class OntologyManagerTest {
    @Test
    public void test() throws OWLOntologyCreationException {
        OntologyManager creator = new OntologyManager();
        creator.createOntology();
        ObjectProperty hasChild = creator.createObjectProperty("Person", "hasChild", "Person");
        creator.createSubclass(hasChild);
        creator.createDefinedClass(hasChild, creator.createConcept("Parent"));
        creator.createEnumeratedClass("Person", "hasGender", "Gender", new String[] {"Male", "Female"});
        ObjectProperty hasGender = creator.createObjectProperty("Person", "hasGender", "Gender");
        creator.createSubclass(hasGender);
        creator.createObjectPropertyAssertion(
                creator.createIndividual(creator.createConcept("Person"), "John_1"),
                hasGender,
                creator.createIndividual(creator.createConcept("Gender"), "Male"));
        creator.createDefinedClassWithIndividual(creator.createConcept("Woman"), hasGender, "Female");
        creator.createObjectPropertyAssertion("Woman", hasGender.getProperty(), "Female");
        creator.checkConsistency();
        assertTrue(creator.checkConsistency());
    }
}
