package experimental;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OntologyManagerTest.class, OntologyReasonerTest.class })
public class AllTests {}
