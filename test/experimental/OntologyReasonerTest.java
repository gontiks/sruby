package experimental;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class OntologyReasonerTest {
    @Test
    public void test() throws OWLOntologyCreationException {
        OntologyReasoner reasoner  = new OntologyReasoner();
        OWLClass parent = reasoner.createConcept("Parent"), person = reasoner.createConcept("Person");
        assertFalse(reasoner.isEquivalent(parent, person));
        assertTrue(reasoner.isEquivalent(reasoner.createConcept("FamilyMember"), reasoner.createConcept("Pet")));
        assertTrue(reasoner.isSubset(parent, person));
        OWLClass mother = reasoner.createConcept("Mother");
        assertFalse(reasoner.isEquivalent(person, reasoner.getComplement(mother)));
        OWLClassExpression[] expressions = new OWLClassExpression[] {reasoner.createConcept("Woman"), reasoner.getComplement(mother)};
        assertFalse(reasoner.isEquivalent(reasoner.getIntersection(expressions), person));
    }
}
