sruby
=====

Semantic Ruby (or SRuby) is a domain-specific language that integrates Ruby with constructs derived from the Semantic Web Stack, most notably OWL, to support semantic reasoning during the execution of Ruby code.
